<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Film::class);
    }

    public function findAllFilmsWithAvgRate(): array
    {
        return $this->createQueryBuilder('f')
            ->select('f.id, f.title, f.director, f.description')
            ->leftJoin('f.ratings', 'r')
            ->addSelect('avg(r.rate) as avg_rating')
            ->groupBy('f.id')
            ->getQuery()
            ->getResult(Query::HYDRATE_SCALAR);
    }

    public function findFilmWithAvgRateById(int $id): array
    {
        return $this->createQueryBuilder('f')
            ->select('f.id, f.title, f.director, f.description')
            ->leftJoin('f.ratings', 'r')
            ->addSelect('avg(r.rate) as avg_rating')
            ->andWhere('f.id = :id')
            ->setParameter('id', $id)
            ->groupBy('f.id')
            ->getQuery()
            ->getResult(Query::HYDRATE_SCALAR);
    }
}
