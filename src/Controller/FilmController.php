<?php namespace App\Controller;

use App\Entity\Film;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    /**
     * @Route(
     *     "/api/film",
     *     name="film_list",
     *     methods={"GET"}
     * )
     */
    public function index(): JsonResponse
    {
        $films = $this->getDoctrine()
            ->getRepository(Film::class)
            ->findAllFilmsWithAvgRate();

        return new JsonResponse($films);
    }

    /**
     * @Route(
     *     "/api/film/{id}",
     *     name="film_element",
     *     methods={"GET"}
     * )
     */
    public function show(int $id): JsonResponse
    {
        $film = $this->getDoctrine()
            ->getRepository(Film::class)
            ->findFilmWithAvgRateById($id);

        if(!$film) {
            return new JsonResponse(sprintf('No film with id: %s', $id), 404);
        }
        return new JsonResponse($film);
    }
}