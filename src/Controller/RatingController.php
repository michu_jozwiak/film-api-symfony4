<?php namespace App\Controller;

use App\Entity\Film;
use App\Entity\Ratings;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RatingController extends AbstractController
{
    /**
     * @Route(
     *     "/api/rating",
     *     name="create_rating",
     *     methods={"POST"}
     * )
     */
    public function create(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Film $film */
        $film = $entityManager->getRepository(Film::class)
            ->find($request->request->get('film_id'));

        if(!$film) {
            return new JsonResponse(
                sprintf('No film wit id: %s', $request->request->get('film_id')), 404);
        }

        $rating = new Ratings();
        $rating->setRate($request->request->get('rate'));

        $film->addRating($rating);

        $entityManager->persist($rating);
        $entityManager->flush();

        return new JsonResponse('OK');
    }
}