#1. Get film list
## [url]/api/film
example response
```json

[
    {
        "id": "1",
        "title": "Die Hard",
        "director": "John McTiernan",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "avg_rating": "5.5000"
    },
    {
        "id": "2",
        "title": "RED",
        "director": "Robert Schwentke",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "avg_rating": "3.6667"
    }
]
```

#2. Get single film
## [url]/api/film/{id} where {id} replace with film id
example response
```json
[
    {
        "id": "1",
        "title": "Die Hard",
        "director": "John McTiernan",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "avg_rating": "5.5000"
    }
]
```

#3 Add film rate
## [url]/api/rating and pass POST data in body request: 
require fields:
- film_id - film id
- rate - rate for film